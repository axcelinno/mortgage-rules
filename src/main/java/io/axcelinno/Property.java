package io.axcelinno;

/**
 * This class was automatically generated by the data modeler tool.
 */

@javax.persistence.Entity
public class Property implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@javax.persistence.GeneratedValue(generator = "PROPERTY_ID_GENERATOR", strategy = javax.persistence.GenerationType.AUTO)
	@javax.persistence.Id
	@javax.persistence.SequenceGenerator(name = "PROPERTY_ID_GENERATOR", sequenceName = "PROPERTY_ID_SEQ")
	private java.lang.Long id;

	@org.kie.api.definition.type.Label(value = "Property Type")
	private java.lang.String propertyType;

	@org.kie.api.definition.type.Label(value = "Property Locale")
	private java.lang.String propertyLocale;

	@org.kie.api.definition.type.Label(value = "Sales Price")
	private java.lang.Integer salesPrice;

	@org.kie.api.definition.type.Label(value = "Property Age")
	private java.lang.Integer propertyAge;

	public Property() {
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public java.lang.String getPropertyType() {
		return this.propertyType;
	}

	public void setPropertyType(java.lang.String propertyType) {
		this.propertyType = propertyType;
	}

	public java.lang.String getPropertyLocale() {
		return this.propertyLocale;
	}

	public void setPropertyLocale(java.lang.String propertyLocale) {
		this.propertyLocale = propertyLocale;
	}

	public java.lang.Integer getSalesPrice() {
		return this.salesPrice;
	}

	public void setSalesPrice(java.lang.Integer salesPrice) {
		this.salesPrice = salesPrice;
	}

	public java.lang.Integer getPropertyAge() {
		return this.propertyAge;
	}

	public void setPropertyAge(java.lang.Integer propertyAge) {
		this.propertyAge = propertyAge;
	}

	public Property(java.lang.Long id, java.lang.String propertyType,
			java.lang.String propertyLocale, java.lang.Integer salesPrice,
			java.lang.Integer propertyAge) {
		this.id = id;
		this.propertyType = propertyType;
		this.propertyLocale = propertyLocale;
		this.salesPrice = salesPrice;
		this.propertyAge = propertyAge;
	}

}