package io.axcelinno;

/**
 * This class was automatically generated by the data modeler tool.
 */

@javax.persistence.Entity
public class Application implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	@javax.persistence.GeneratedValue(generator = "APPLICATION_ID_GENERATOR", strategy = javax.persistence.GenerationType.AUTO)
	@javax.persistence.Id
	@javax.persistence.SequenceGenerator(name = "APPLICATION_ID_GENERATOR", sequenceName = "APPLICATION_ID_SEQ")
	private java.lang.Long id;

	@org.kie.api.definition.type.Label("Applicant")
	@javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.EAGER, cascade = {javax.persistence.CascadeType.ALL})
	private io.axcelinno.Applicant applicant;

	@org.kie.api.definition.type.Label("Down Payment")
	private java.lang.Integer downPayment;

	@org.kie.api.definition.type.Label("Amortization")
	private java.lang.Integer amortization;

	@org.kie.api.definition.type.Label("Mortgage Amount")
	private java.lang.Integer mortgageAmount;

	@javax.persistence.ManyToOne(fetch = javax.persistence.FetchType.EAGER, cascade = {javax.persistence.CascadeType.ALL})
	@org.kie.api.definition.type.Label("Property")
	private io.axcelinno.Property property;

	@org.kie.api.definition.type.Label("Mortgage APR")
	private Double mortgageApr;

	@org.kie.api.definition.type.Label(value = "Income Points")
	private java.lang.Double incomePoints;

	@org.kie.api.definition.type.Label(value = "Credit Points")
	private java.lang.Double creditPoints;

	@org.kie.api.definition.type.Label(value = "Education Points")
	private java.lang.Double educationPoints;

	public Application() {
	}

	public java.lang.Long getId() {
		return this.id;
	}

	public void setId(java.lang.Long id) {
		this.id = id;
	}

	public io.axcelinno.Applicant getApplicant() {
		return this.applicant;
	}

	public void setApplicant(io.axcelinno.Applicant applicant) {
		this.applicant = applicant;
	}

	public java.lang.Integer getDownPayment() {
		return this.downPayment;
	}

	public void setDownPayment(java.lang.Integer downPayment) {
		this.downPayment = downPayment;
	}

	public java.lang.Integer getAmortization() {
		return this.amortization;
	}

	public void setAmortization(java.lang.Integer amortization) {
		this.amortization = amortization;
	}

	public java.lang.Integer getMortgageAmount() {
		return this.mortgageAmount;
	}

	public void setMortgageAmount(java.lang.Integer mortgageAmount) {
		this.mortgageAmount = mortgageAmount;
	}

	public io.axcelinno.Property getProperty() {
		return this.property;
	}

	public void setProperty(io.axcelinno.Property property) {
		this.property = property;
	}

	public java.lang.Double getMortgageApr() {
		return this.mortgageApr;
	}

	public void setMortgageApr(java.lang.Double mortgageApr) {
		this.mortgageApr = mortgageApr;
	}

	public java.lang.Double getIncomePoints() {
		return this.incomePoints;
	}

	public void setIncomePoints(java.lang.Double incomePoints) {
		this.incomePoints = incomePoints;
	}

	public java.lang.Double getCreditPoints() {
		return this.creditPoints;
	}

	public void setCreditPoints(java.lang.Double creditPoints) {
		this.creditPoints = creditPoints;
	}

	public java.lang.Double getEducationPoints() {
		return this.educationPoints;
	}

	public void setEducationPoints(java.lang.Double educationPoints) {
		this.educationPoints = educationPoints;
	}

	public Application(java.lang.Long id, io.axcelinno.Applicant applicant,
			java.lang.Integer downPayment, java.lang.Integer amortization,
			java.lang.Integer mortgageAmount, io.axcelinno.Property property,
			java.lang.Double mortgageApr, java.lang.Double incomePoints,
			java.lang.Double creditPoints, java.lang.Double educationPoints) {
		this.id = id;
		this.applicant = applicant;
		this.downPayment = downPayment;
		this.amortization = amortization;
		this.mortgageAmount = mortgageAmount;
		this.property = property;
		this.mortgageApr = mortgageApr;
		this.incomePoints = incomePoints;
		this.creditPoints = creditPoints;
		this.educationPoints = educationPoints;
	}

}